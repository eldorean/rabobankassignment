import { Text } from '@src/components/Text'
import React from 'react'
import { RabobankPage } from '../Page/RabobankPage'

const HomeScreen: React.FC = () => (
  <RabobankPage>
    <Text size="xl">Welkom bij de Rabobank</Text>
  </RabobankPage>
)

export { HomeScreen }
