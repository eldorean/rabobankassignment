import React from 'react'

const ScreenMediator = <T1, T2>(
  view: React.FC<T2>,
  state: (props: T1) => T2
):React.FC<T1> => (props) => view(state(props))

export { ScreenMediator }
