import React, { CSSProperties } from 'react'
import { RabobankPage } from '../Page/RabobankPage'
import { formRow, Input } from './components/Input'
import { RegisterViewProps } from './interfaces'

const formStyle: CSSProperties = {
  margin: 'auto',
  maxWidth: 400
}

const formRowButton: CSSProperties = {
  ...formRow,
  paddingLeft: '40px'
}

const formButton: CSSProperties = {
  backgroundColor: '#ffffff',
  borderColor: '#009',
  padding: '12px 24px',
  margin: 'auto',
  borderRadius: '4px',
  cursor: 'pointer'
}

const RegisterView: React.FC<RegisterViewProps> =
  ({ firstNameInput, lastNameInput, emailInput, passwordInput, onSubmit }) => (
  <RabobankPage>
    <div style={formStyle}>
      <Input {...firstNameInput} placeholder="First name: " />
      <Input {...lastNameInput} placeholder="Last name: " />
      <Input type="email" {...emailInput} placeholder="Email: " />
      <Input type="password" {...passwordInput} placeholder="Password: " />
      <div style={formRowButton}>
        <button style={formButton} onClick={onSubmit}>Submit</button>
      </div>
    </div>
  </RabobankPage>
  )

export { RegisterView }
