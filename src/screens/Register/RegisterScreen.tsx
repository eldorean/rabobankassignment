import { FieldChecks, UserSubmitService } from '@src/core/domain/PostUser'
import { UserPostServiceImpl, UserRequestServiceImpl } from '@src/services/UserServices'
import { NotEmptyCheck } from '@src/core/domain/Check/Check'
import { PasswordCheck } from '@src/core/domain/Check/PasswordCheck'
import { ScreenMediator } from '../ScreenMediator'
import { RegisterState } from './RegisterState'
import { RegisterView } from './RegisterView'

const fieldChecks: FieldChecks = ({
  firstName: new NotEmptyCheck(),
  lastName: new NotEmptyCheck(),
  email: new NotEmptyCheck(),
  password: new PasswordCheck([''])
})

const userSubmitService = new UserSubmitService(
  fieldChecks,
  new UserRequestServiceImpl(),
  new UserPostServiceImpl()
)

const RegisterScreenImpl = ScreenMediator(RegisterView, RegisterState)
const RegisterScreen = () => RegisterScreenImpl(userSubmitService)

export { RegisterScreen }
