import React, { useState } from 'react'
import { FormField, formFieldsEnum, UserSubmitService, FormValues } from '@src/core/domain/PostUser'
import { RegisterInputState, RegisterViewProps } from './interfaces'

type FieldStates = Record<FormField, RegisterInputState>

type InputChangeFactory = (field: FormField) =>
  React.ChangeEventHandler<HTMLInputElement>

type ErrorChangeAction = (field: FormField, errors: string[]) => void

const generateDefaultState = () => ({
  value: '',
  errorMessages: undefined
})

const defaultState: FieldStates = {
  firstName: generateDefaultState(),
  lastName: generateDefaultState(),
  email: generateDefaultState(),
  password: generateDefaultState()
}

const RegisterState = (service: UserSubmitService): RegisterViewProps => {
  const [fieldsState, setFieldsState] = useState(defaultState)

  const ResetErrorMessages = () => setFieldsState(pref => ({
    firstName: { ...pref.firstName, errorMessages: [] },
    lastName: { ...pref.lastName, errorMessages: [] },
    email: { ...pref.email, errorMessages: [] },
    password: { ...pref.password, errorMessages: [] }
  }))

  const getValuesFromState = () =>
    Object.fromEntries(
      Object.entries(fieldsState)
        .map(([key, value]) => ([key, value.value]))
    ) as FormValues

  const updateFieldValue: InputChangeFactory = (field) => (e) => {
    const state = fieldsState[field]
    const updatedState = { [field]: { ...state, value: e.target.value } }
    setFieldsState(pref => ({ ...pref, ...updatedState }))
    ResetErrorMessages()
  }

  const updateFieldErrors: ErrorChangeAction = (field, errors) => {
    const state = fieldsState[field]
    const updatedState = { [field]: { ...state, errorMessages: errors } }
    setFieldsState(pref => ({ ...pref, ...updatedState }))
  }

  const onSubmit = async () => {
    const checks = service.formFieldValidate(getValuesFromState())
    const isValid = Object.values(checks).every(check => check.valid)
    if (isValid) {
      const response = await service.ProcesForm(getValuesFromState())
      console.log(response)
    } else {
      formFieldsEnum.forEach(key => updateFieldErrors(key, checks[key].errorMessage))
    }
  }

  return ({
    firstNameInput: { ...fieldsState.firstName, onChange: updateFieldValue('firstName') },
    lastNameInput: { ...fieldsState.lastName, onChange: updateFieldValue('lastName') },
    emailInput: { ...fieldsState.email, onChange: updateFieldValue('email') },
    passwordInput: { ...fieldsState.password, onChange: updateFieldValue('password') },
    onSubmit
  })
}

export { RegisterState }
