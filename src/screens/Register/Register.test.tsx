import { UserSubmitService } from '@src/core/domain/PostUser'
import { RegisterState } from './RegisterState'

const mock: UserSubmitService = {
//   formFieldValidate: () => ({
//     firstName: { valid: true, errorMessage: [], errorCode: [] },
//     lastName: { valid: true, errorMessage: [], errorCode: [] },
//     email: { valid: true, errorMessage: [], errorCode: [] },
//     password: { valid: true, errorMessage: [], errorCode: [] }
//   }),
//   ProcesForm: async () => new User({ firstName: 'John', lastName: 'Doe', email: 'test@test.nl' })
}

describe('RegisterState', () => {
  const registerState = RegisterState(mock)

  test('registerState updates', () => {
    registerState.firstNameInput.onChange({ target: { value: 'b' } })

    console.log(registerState.firstNameInput.value)
  })
})
