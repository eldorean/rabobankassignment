import React, { CSSProperties } from 'react'
import { BasicInputError } from '@src/components/Input/InputError'
import { RegisterInputState } from '../interfaces'

export const formRow: CSSProperties = {
  padding: '8px'
}

export const formInput: CSSProperties = {
  width: '100%',
  padding: '6px 8px',
  border: '1px solid #333',
  borderRadius: '4px'
}

const Input: React.FC<RegisterInputState & React.HTMLProps<HTMLInputElement>> =
  ({ placeholder, value, onChange, errorMessages, ...props }) => (
  <div style={formRow}>
    <input {...props} style={formInput} {...{ placeholder, value, onChange } }/>
    <BasicInputError message={errorMessages} />
  </div>
  )

export { Input }
