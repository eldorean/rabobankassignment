import { ErrorMessage } from '@src/components/Input/InputError'

export interface RegisterInputState {
  value: string,
  errorMessages: ErrorMessage
}

export interface RegisterInputView extends RegisterInputState {
  onChange: React.ChangeEventHandler<HTMLInputElement>
}

export interface RegisterViewProps {
  firstNameInput: RegisterInputView
  lastNameInput: RegisterInputView
  emailInput: RegisterInputView
  passwordInput: RegisterInputView
  onSubmit: () => void
}
