import React from 'react'
import { Content, RabobankNavBar } from './components'

const RabobankPage: React.FC = ({ children }) => (
  <div>
    <RabobankNavBar />
    <Content>{children}</Content>
  </div>
)

export { RabobankPage }
