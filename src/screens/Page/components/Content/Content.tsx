import React, { CSSProperties } from 'react'

const style: CSSProperties = {
  maxWidth: '960px',
  margin: 'auto',
  padding: '20px'
}

const Content: React.FC = ({ children }) => (
  <div style={style}>{children}</div>
)

export { Content }
