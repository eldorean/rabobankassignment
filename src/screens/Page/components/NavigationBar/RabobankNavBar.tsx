
import React from 'react'
import { Rabobank } from '@src/components/Icon/RabobankIcon'
import { Bar, Logo, Link as LinkStyle } from '@src/components/NavigationBar/styles'
import { Link } from '@src/components/NavigationBar/components'
import { Text } from '@src/components/Text'

const barStyle = Bar
const logoStyle = Logo
const linkStyle = LinkStyle

const RabobankNavBar: React.FC = () => (
  <div style={barStyle}>
    <div style={logoStyle}><Rabobank /></div>
    <Link to="/" style={linkStyle} >
      <Text size="l">Home</Text>
    </Link>
    <Link to="/register" style={linkStyle}>
      <Text size="l">Register</Text>
    </Link>
  </div>
)

export { RabobankNavBar }
