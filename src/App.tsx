import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { HomeScreen } from './screens'
import { RegisterScreen } from './screens/Register'
import './globalStyle.css'

const App: React.FC = () => (
   <Router>
      <Switch>
         <Route path="/register">
            <RegisterScreen />
         </Route>
         <Route path="/">
            <HomeScreen />
         </Route>
      </Switch>
   </Router>
)

export default App
