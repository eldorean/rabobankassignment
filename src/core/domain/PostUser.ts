import { Sleep } from '@src/util/Sleep'
import { Check, CheckResult } from '../entities/check'
import { User } from '../entities/User'
import { NotEmptyCheck } from './Check/Check'
import { PasswordCheck } from './Check/PasswordCheck'

export const formFieldsEnum = ['firstName', 'lastName', 'email', 'password'] as const
export type FormField = typeof formFieldsEnum[number]

export type FormValues = Record<FormField, string>

export type UserPostKeys = FormField
export type UserPostObject = Record<UserPostKeys, string>
export interface UserPostService {
  post: (body: UserPostObject) => Promise<void>
}

export type UserRequestKeys = Exclude<FormField, 'password'>
export type UserRequestObject = Record<UserRequestKeys, string>
export interface UserRequestService {
  request: () => Promise<UserRequestObject>
}

export type FieldChecks = Record<FormField, Check<string>>

class UserSubmitService {
  constructor (
    private formFields: FieldChecks,
    private UserRequestService: UserRequestService,
    private UserPostService: UserPostService
  ) {}

  public formFieldValidate = (user: FormValues): Record<FormField, CheckResult> => ({
    firstName: new NotEmptyCheck().check(user.firstName),
    lastName: new NotEmptyCheck().check(user.lastName),
    email: new NotEmptyCheck().check(user.email),
    password: new PasswordCheck([user.firstName, user.lastName]).check(user.password)
  });

  ProcesForm = async (user: FormValues) => {
    this.formFieldsAreValid(user)
    try {
      await this.UserPostService.post(user)
      await Sleep(4000)
      return new User(await this.UserRequestService.request())
    } catch (e) {
      throw new Error('Unable to resolve request.')
    }
  }

  private formFieldsAreValid: (user: FormValues) => asserts user = (user) => {
    const isValid = this.formFieldValidate(user)
    !isValid && this.FieldError()
  }

  private FieldError = () => { throw new Error('Incoming fields are not valid. Unable to proces request.') }
}

export { UserSubmitService }
