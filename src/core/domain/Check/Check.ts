import { Check, CheckAction } from '@src/core/entities/check'

const generateResponse = (
  condition: boolean,
  errorMessages: string[],
  errorCodes: number[]
) => ({
  errorMessage: !condition ? errorMessages : [],
  errorCode: !condition ? errorCodes : [],
  valid: condition
})

class NotEmptyCheck<T> implements Check<T> {
  check: CheckAction<T> = (value: T) =>
    generateResponse(
      !!value,
      ['Field is empty'],
      [1001]
    );
}

class CharCountCheck implements Check<string> {
  constructor (private len: number) {}
  check: CheckAction<string> = (value: string) =>
    generateResponse(
      value.length >= this.len,
      ['Not enough characters.'],
      [1002]
    );
}

class HasUpperCaseCheck implements Check<string> {
  check: CheckAction<string> = (value: string) =>
    generateResponse(
      value.split('').some(char => char === char.toUpperCase()),
      ['No uppercase characters found.'],
      [1003]
    );
}

class HasLowerCaseCheck implements Check<string> {
  check: CheckAction<string> = (value: string) =>
    generateResponse(
      value.split('').some(char => char === char.toLowerCase()),
      ['No lowercase characters found.'],
      [1004]
    );
}

class ShouldNotContainCheck implements Check<string> {
  constructor (private target: string) {}
  check: CheckAction<string> = (value: string) =>
    generateResponse(
      !value.includes(this.target),
      [`stringValue contains the string ${this.target}`],
      [1005]
    );
}

export {
  NotEmptyCheck, CharCountCheck, HasUpperCaseCheck, HasLowerCaseCheck, ShouldNotContainCheck
}
