import { CheckResult } from '@src/core/entities/check'

export class MultiCheckState {
  private responses: CheckResult[] = [];
  AddResponse = (response: CheckResult) => {
    this.responses.push(response)
  };

  getState = (): CheckResult => ({
    errorMessage: this.getErrorMessages(),
    errorCode: this.getErrorCodes(),
    valid: this.isValid()
  });

  private getErrorMessages = () => this.responses.map(r => r.errorMessage).flat()
  private getErrorCodes = () => this.responses.map(r => r.errorCode).flat()
  private isValid = () => this.responses.every(r => r.valid)
}
