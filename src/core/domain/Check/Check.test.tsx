import {
  NotEmptyCheck,
  CharCountCheck,
  HasUpperCaseCheck,
  HasLowerCaseCheck,
  ShouldNotContainCheck
} from './Check'

describe('testing validation checks', () => {
  describe('NotEmptyCheck checks for empty values', () => {
    const checker = new NotEmptyCheck<string>()
    test('it return invalid when string is empty', () => {
      const actual = checker.check('')
      expect(actual.valid).toBe(false)
      expect(actual.errorMessage.length).toBe(1)
      expect(actual.errorCode.length).toBe(1)
    })

    test('it return valid when string is filled', () => {
      const actual = checker.check('testing')
      expect(actual.valid).toBe(true)
      expect(actual.errorMessage.length).toBe(0)
      expect(actual.errorCode.length).toBe(0)
    })
  })

  describe('CharCountCheck checks for minimum charactor count', () => {
    const checker = new CharCountCheck(8)
    test('it return invalid when string is to small', () => {
      const actual = checker.check('small')
      expect(actual.valid).toBe(false)
      expect(actual.errorMessage.length).toBe(1)
      expect(actual.errorCode.length).toBe(1)
    })

    test('it return valid when string is long enough', () => {
      const actual = checker.check('extraLong')
      expect(actual.valid).toBe(true)
      expect(actual.errorMessage.length).toBe(0)
      expect(actual.errorCode.length).toBe(0)
    })
  })

  describe('HasUpperCaseCheck checks for atleast 1 Uppercase character', () => {
    const checker = new HasUpperCaseCheck()
    test('it return invalid when string has no uppercase character', () => {
      const actual = checker.check('lowercase')
      expect(actual.valid).toBe(false)
      expect(actual.errorMessage.length).toBe(1)
      expect(actual.errorCode.length).toBe(1)
    })

    test('it return valid when string has a uppercase character', () => {
      const actual = checker.check('Camelcase')
      expect(actual.valid).toBe(true)
      expect(actual.errorMessage.length).toBe(0)
      expect(actual.errorCode.length).toBe(0)
    })
  })

  describe('HasUpperCaseCheck checks for atleast 1 Lowercase character', () => {
    const checker = new HasLowerCaseCheck()
    test('it return invalid when string has no lowercase character', () => {
      const actual = checker.check('UPPERCASE')
      expect(actual.valid).toBe(false)
      expect(actual.errorMessage.length).toBe(1)
      expect(actual.errorCode.length).toBe(1)
    })

    test('it return valid when string has a lowercase character', () => {
      const actual = checker.check('Camelcase')
      expect(actual.valid).toBe(true)
      expect(actual.errorMessage.length).toBe(0)
      expect(actual.errorCode.length).toBe(0)
    })
  })

  describe('ShouldNotContainCheck checks if value string does not contain target string', () => {
    const checker = new ShouldNotContainCheck('target')
    test('it return invalid when target string is present', () => {
      const actual = checker.check('pretargetpre')
      expect(actual.valid).toBe(false)
      expect(actual.errorMessage.length).toBe(1)
      expect(actual.errorCode.length).toBe(1)
    })

    test('it return valid when target string is not present', () => {
      const actual = checker.check('pre*argetpre')
      expect(actual.valid).toBe(true)
      expect(actual.errorMessage.length).toBe(0)
      expect(actual.errorCode.length).toBe(0)
    })
  })
})
