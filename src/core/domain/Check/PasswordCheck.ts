
import { Check } from '@src/core/entities/check'
import { CharCountCheck, HasLowerCaseCheck, HasUpperCaseCheck, NotEmptyCheck, ShouldNotContainCheck } from './Check'
import { MultiCheckState } from './MultiCheckState'

class PasswordCheck implements Check<string> {
  private nameCheck: Check<string>[];
  constructor (nameTargets: string[]) {
    this.nameCheck = nameTargets.map(t => new ShouldNotContainCheck(t))
  }

  private Checks: Check<string>[] = [
    new NotEmptyCheck(),
    new CharCountCheck(8),
    new HasUpperCaseCheck(),
    new HasLowerCaseCheck()
  ];

  check = (password: string) => {
    const state = new MultiCheckState()
    const checks = this.GetChecks()

    checks.forEach(check => {
      const response = check.check(password)
      !response.valid && state.AddResponse(response)
    })
    return state.getState()
  }

  private GetChecks = () => [...this.Checks, ...this.nameCheck];
}

export { PasswordCheck }
