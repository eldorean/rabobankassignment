export type CheckAction<T> = (value: T) => CheckResult
export interface CheckResult {
  errorMessage: string[],
  errorCode: number[],
  valid: boolean,
}

export interface Check<T> {
  check: CheckAction<T>;
}
