export interface UserProps {
  firstName: string;
  lastName: string;
  email: string;
}
interface User extends UserProps{}

class User {
  constructor (props: UserProps) { Object.assign(this, props) }

  GetFullName = () => `${this.firstName} ${this.lastName}`
}

export { User }
