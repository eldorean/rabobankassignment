import { ArialFont } from './styles/Arial'
import { TextGenerator } from './Text'

const PrimaryText = TextGenerator(ArialFont)

const Text = PrimaryText

export { Text }
