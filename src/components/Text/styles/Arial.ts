import { CSSProperties } from 'react'
export type CssFontFamily = Pick<CSSProperties, 'fontFamily'>;

const ArialFont: CssFontFamily = {
  fontFamily: 'arial'
}

export { ArialFont }
