import { CSSProperties } from 'react'

export type FontSize = 'xs' | 's' | 'm' | 'l' | 'xl';
export type CssFontSize = Pick<CSSProperties, 'fontSize'>;
export type FontSizeMap = Record<FontSize, CssFontSize>

const generaleFontStyle = (int: number): CssFontSize => ({ fontSize: `${int}px` })

export const SizeMap: FontSizeMap = {
  xs: generaleFontStyle(8),
  s: generaleFontStyle(10),
  m: generaleFontStyle(14),
  l: generaleFontStyle(18),
  xl: generaleFontStyle(22)
}
