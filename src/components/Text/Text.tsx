import React, { CSSProperties } from 'react'
import { CssFontFamily } from './styles/Arial'

import { FontSize, SizeMap } from './styles/Size'

interface TextProps {
  size?: FontSize;
  bold?: number;
  italic?: boolean;
  underlined?: boolean;
}

const TextGenerator = (font: CssFontFamily): React.FC<TextProps> =>
  ({ size, bold, italic, underlined, children }) => {
    const style: CSSProperties = {
      ...font,
      ...SizeMap[size || 'm'],
      fontWeight: bold || 400,
      textDecoration: underlined ? 'underline' : 'none',
      fontStyle: italic ? 'italic' : 'none'
    }

    return <span style={style} >{children}</span>
  }

export { TextGenerator }
