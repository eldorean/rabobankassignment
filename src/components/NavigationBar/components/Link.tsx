import React from 'react'
import { NavLink, NavLinkProps } from 'react-router-dom'
import { Text } from '@src/components/Text'

const Link: React.FC<NavLinkProps> = ({ children, ...props }) => (
  <NavLink {...props}><Text size="l">{children}</Text></NavLink>
)

export { Link }
