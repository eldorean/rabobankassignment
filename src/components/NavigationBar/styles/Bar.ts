import { CSSProperties } from 'react'

const Bar: CSSProperties = {
  display: 'flex',
  flexDirection: 'row',
  padding: '15px',
  alignItems: 'center',
  borderBottom: '1px solid #000'
}

export { Bar }
