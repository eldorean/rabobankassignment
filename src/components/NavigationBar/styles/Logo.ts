import { CSSProperties } from 'react'

const Logo: CSSProperties = {
  flex: 0,
  display: 'flex',
  padding: '5px',
  marginRight: '10px'
}

export { Logo }
