import { CSSProperties } from 'react'

const Link: CSSProperties = {
  flex: 0,
  display: 'flex',
  padding: '5px',
  textDecoration: 'none',
  color: '#333'
}

export { Link }
