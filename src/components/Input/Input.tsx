import { CSSProperties } from 'react'

const InputStyle: CSSProperties = {
  width: '100%',
  padding: '6px 8px',
  border: '1px solid #333',
  borderRadius: '4px'
}

export { InputStyle }
