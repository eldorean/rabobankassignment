import { CSSProperties } from 'react'

export const InputError: CSSProperties = {
  display: 'inline-block',
  color: '#e84855',
  backgroundColor: '#FFF',
  border: '2px solid #e84855',
  marginTop: '5px',
  padding: '4px',
  borderRadius: '4px'
}
