import React from 'react'
import { Text } from '../Text'
import { InputError } from './style/InputError'

export type ErrorMessage = string | string[] | undefined

interface InputErrorProps {
  message?: ErrorMessage
}
type InputError = React.FC<InputErrorProps>

const BasicInputErrorMessage:React.FC<{message: string}> = ({ message }) => (
  <div>
    <div style={InputError}>
      <Text size="s">{message}</Text>
    </div>
  </div>
)

const BasicInputError: InputError = ({ message }) => {
  if (!message) return null
  const messageArray = ([] as string[]).concat(message)
  const messageComp = messageArray.map((e, i) => <BasicInputErrorMessage message={e} key={`${e}-${i}`} />)

  return <div >{messageComp}</div>
}

export { BasicInputError }
