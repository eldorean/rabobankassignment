import { UserPostObject, UserRequestObject } from '@src/core/domain/PostUser'

const serviceUri = 'https://demo-api.now.sh/users'

class UserPostServiceImpl implements UserPostServiceImpl {
  post = async (body: UserPostObject) => {
    fetch(serviceUri, {
      method: 'POST',
      body: JSON.stringify(body)
    })
  }
}

class UserRequestServiceImpl implements UserRequestServiceImpl {
  request = async () => {
    const response = await fetch(serviceUri)
    const json = await response.json()
    return json[0] as UserRequestObject
  }
}

export { UserPostServiceImpl, UserRequestServiceImpl }
