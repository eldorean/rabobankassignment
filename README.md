# RabobankAssignment

## Getting Started
### Installing
install this project (I used yarn)
```
yarn install
```

### Developing
After installing you can use:
```
yarn start
```

when you do this: webpack will start serving the web application to your browser.

### Building
```
yarn build
```

### Testing
```
yarn test
```

## Tooling that I used
I used the following tooling: 
* React
* typescript
* webpack 
* babel
* eslint
* jest
* enzyme


## Architecture
I have chosen for a domain driven architecture, building from a business solution is in my opinion the best way to build a solid and strong code base.
Offcoarse it`s probably not the the best solution for this project becausse of the complexity that comes with it. 


## Estimation
So as the assignment said it's shouldn't take more that 4 hours. Well it took me a little more. 
Mainly the new webpack 5 gave me some hassle, so the boilerplate took longer than i had hoped. 
Also whenever I have to do an assignment I like to try some new things and experiment some with the code.
so overal I think i was 4 to 5 hours working on this project.

## Side-notes
So the assignment didn't explain what to do with the response. So I just write it away in a console log.
Also the UX is clear and understandable, but probebly would win in a beauty contest. But this wasn't the main focus of the assignment so I kept it simple.
And last but not least a little todo:. I have Jest working partially but it's not importing the modules correctly. I need to have a look at that still. 



Hope you'll enjoy it as much as I enjoyed building it.